/*
 * Copyright 2018 carddamom.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
module cc.chordflower.ellie {
	requires java.base;
	requires javafx.base;
	requires javafx.controls;
	requires javafx.graphics;
	requires javafx.media;
	requires javafx.web;
	requires feather;
	requires javax.inject;
	requires java.validation;
	requires org.hibernate.validator;
	requires org.apache.commons.codec;
	requires jodd.core;
	requires jodd.mail;
	requires javax.mail;
	requires jena.iri;
	requires jena.core;
	requires jena.base;
	requires jena.shaded.guava;
	requires commons.csv;
	requires commons.lang;
	requires miglayout.javafx;
	requires miglayout.core;
	requires controlsfx;
	requires undofx;
	requires reactfx;
	requires org.slf4j;
	requires orientdb.core;
	requires fontawesomefx;
}
