/*
 * Copyright 2018 carddamom.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.chordflower.ellie.utils;

/**
 * This is a class to reatrieve a class loader to use to load resources for example.
 *
 * Based on [this
 * article](https://www.javaworld.com/article/2077344/core-java/find-a-way-out-of-the-classloader-maze.html)
 *
 * @author carddamom
 */
public class ClassLoaderResolver {

	/**
	 * Finds is the given parent has the given child (at any sublevel of its hierarchy).
	 *
	 * @param parent The parent classloader.
	 * @param child The child classloader.
	 * @return True if the child has the given parent somewhere in its hierarchy, false otherwise (meaning they are
	 * siblings.)
	 */
	private static boolean isChild(ClassLoader parent, ClassLoader child) {
		if (child.getParent() == null) {
			return false;
		}
		if (child.getParent().equals(parent)) {
			return true;
		}
		return isChild(parent, child.getParent());
	}

	private ClassLoaderResolver() {

	}

	public static ClassLoader getClassLoader(Class currentClass) {

		ClassLoader currentLoader = currentClass.getClassLoader();
		ClassLoader contextLoader = Thread.currentThread().getContextClassLoader();
		ClassLoader systemLoader = ClassLoader.getSystemClassLoader();

		if (isChild(contextLoader, currentLoader)) {
			if (isChild(currentLoader, systemLoader)) {
				return systemLoader;
			}
			return currentLoader;
		}
		else if (isChild(currentLoader, contextLoader)) {
			if (isChild(contextLoader, systemLoader)) {
				return systemLoader;
			}
			return contextLoader;
		}
		else {
			if (isChild(contextLoader, systemLoader)) {
				return systemLoader;
			}
			return contextLoader;
		}
	}

}
