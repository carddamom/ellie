/*
 * Copyright 2018 carddamom.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.chordflower.ellie;

import cc.chordflower.ellie.utils.ClassLoaderResolver;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * This is the main class of Ellie.
 *
 * @author carddamom
 */
public class EllieMain extends Application {

	private static final Logger LOG = Logger.getLogger(EllieMain.class.getName());

	public EllieMain() {
		EllieMain.setUserAgentStylesheet(
						ClassLoaderResolver.getClassLoader(EllieMain.class).getResource("modena_dark.css").toString());
	}

	@Override
	public void start(Stage stageLeft) throws Exception {
		Parent root = FXMLLoader.load(ClassLoaderResolver.getClassLoader(EllieMain.class).getResource("main.fxml"));
		Scene scene = new Scene(root, 800, 600);
		scene.setUserAgentStylesheet(
						ClassLoaderResolver.getClassLoader(EllieMain.class).getResource("modena_dark.css").toString());
		stageLeft.setTitle("Ellie");
		stageLeft.setScene(scene);
		stageLeft.show();
	}

	@Override
	public void stop() throws Exception {

	}

	@Override
	public void init() throws Exception {

	}

	public static void main(String[] args) {
		EllieMain.launch(args);
	}

}
