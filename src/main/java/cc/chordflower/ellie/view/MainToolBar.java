/*
 * Copyright ${YEAR} carddamom
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.chordflower.ellie.view;

import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.Separator;
import javafx.scene.control.ToolBar;

public class MainToolBar extends ToolBar {

	public MainToolBar( ){
		super(
				new Button( "New" ),
				new Button( "Open" ),
				new Button( "Close" ),
				new Button( "Save" ),
				new Separator(Orientation.HORIZONTAL),
				new Button( "Print" ),
				new Button( "Print Properties" ),
				new Button( "Properties" ),
				new Separator(Orientation.HORIZONTAL),
				new Button( "Exit" ),
				new Separator(Orientation.HORIZONTAL),
				new Button( "Undo" ),
				new Button( "Redo" ),
				new Separator(Orientation.HORIZONTAL),
				new Button( "Cut" ),
				new Button( "Copy" ),
				new Button( "Paste" ),
				new Button( "Delete" ),
				new Separator(Orientation.HORIZONTAL),
				new Button( "Find" ),
				new Button( "Find Next" ),
				new Button( "Replace" ),
				new Separator(Orientation.HORIZONTAL),
				new Button( "Next Tab" ),
				new Button( "Previous Tab" ),
				new Button( "Tab List" ),
				new Separator(Orientation.HORIZONTAL),
				new Button( "Help" ),
				new Button( "Contents" ),
				new Button( "Version" )
		);
	}

}
