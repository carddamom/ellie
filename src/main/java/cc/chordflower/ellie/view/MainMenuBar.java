/*
 * Copyright ${YEAR} carddamom
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.chordflower.ellie.view;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;

public class MainMenuBar extends MenuBar {

	public MainMenuBar( ){
		super();

		Menu file = new Menu();
		file.getItems().add( new MenuItem( "New" ) );
		file.getItems().add( new MenuItem( "Open" ) );
		file.getItems().add( new SeparatorMenuItem() );
		file.getItems().add( new MenuItem( "Close" ) );
		file.getItems().add( new MenuItem( "Save" ) );
		file.getItems().add( new MenuItem( "Save As" ) );
		file.getItems().add( new MenuItem( "Restore" ) );
		file.getItems().add( new SeparatorMenuItem() );
		file.getItems().add( new MenuItem( "Print" ) );
		file.getItems().add( new MenuItem( "Print Properties" ) );
		file.getItems().add( new MenuItem( "Properties" ) );
		file.getItems().add( new SeparatorMenuItem() );
		file.getItems().add( new MenuItem( "Exit" ) );

		Menu edit = new Menu();
		edit.getItems().add( new MenuItem( "Undo" ) );
		edit.getItems().add( new MenuItem( "Redo" ) );
		edit.getItems().add( new SeparatorMenuItem() );
		edit.getItems().add( new MenuItem( "Cut" ) );
		edit.getItems().add( new MenuItem( "Copy" ) );
		edit.getItems().add( new MenuItem( "Paste" ) );
		edit.getItems().add( new MenuItem( "Delete" ) );
		edit.getItems().add( new SeparatorMenuItem() );
		edit.getItems().add( new MenuItem( "Select All" ) );
		edit.getItems().add( new SeparatorMenuItem() );
		edit.getItems().add( new MenuItem( "Find" ) );
		edit.getItems().add( new MenuItem( "Find Next" ) );
		edit.getItems().add( new MenuItem( "Replace" ) );

		Menu window = new Menu();
		window.getItems().add( new MenuItem( "Minimize" ) );
		window.getItems().add( new MenuItem( "Zoom" ) );
		window.getItems().add( new SeparatorMenuItem() );
		window.getItems().add( new MenuItem( "Next Tab" ) );
		window.getItems().add( new MenuItem( "Previous Tab" ) );
		window.getItems().add( new MenuItem( "Tab List" ) );

		Menu help = new Menu();
		help.getItems().add( new MenuItem( "Help" ) );
		help.getItems().add( new MenuItem( "Contents" ) );
		help.getItems().add( new SeparatorMenuItem() );
		help.getItems().add( new MenuItem( "Version" ) );

		this.getMenus().add( file );
		this.getMenus().add( edit );
		this.getMenus().add( window );
		this.getMenus().add( help );
	}
}
