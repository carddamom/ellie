/*
 * Copyright ${YEAR} carddamom
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.chordflower.ellie.view;

import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import org.tbee.javafx.scene.layout.MigPane;

import java.time.LocalDate;

public class TaxonomyDetails extends ScrollPane {

	public TaxonomyDetails( ){
		super();

		/**
		 * Title              Alternative Title
		 * Identifier
		 * Description
		 *
		 * Date Created       Date Submitted      Date Accepted
		 * Date Available     Date Modified       Date Valid
		 * Creator            Contributor         Publisher
		 *
		 * Conforms To        Has Part            Has Version
		 * Conforms To        Has Part            Has Version
		 *
		 * Is Part Of         Is Referenced By    Is Replaced By
		 * Is Part Of         Is Referenced By    Is Replaced By
		 * Is Required By
		 * Is Required By
		 *
		 * References         Relation            Replaces
		 * References         Relation            Replaces
		 * Requires           Source
		 * Requires           Source
		 */

		MigPane layout = new MigPane(  );

		layout.add( new Label("Title") );
		TextField title = new TextField(  );
		layout.add( title, "span 2" );

		layout.add( new Label("Alternative Title") );
		TextField alternativeTitle = new TextField(  );
		layout.add( alternativeTitle, "span 2, wrap" );

		layout.add( new Label("Identifier") );
		TextField identifier = new TextField(  );
		layout.add( identifier, "growx, wrap" );

		layout.add( new Label("Description") );
		TextField description = new TextField(  );
		layout.add( description, "growx, wrap" );

		layout.add( new Label("Date Created") );
		DatePicker dateCreated = new DatePicker( LocalDate.now() );
		layout.add( dateCreated, "span 2" );

		layout.add( new Label("Date Submitted") );
		DatePicker dateSubmitted = new DatePicker( LocalDate.now() );
		layout.add( dateSubmitted, "span 2" );

		layout.add( new Label("Date Accepted") );
		DatePicker dateAccepted = new DatePicker( LocalDate.now() );
		layout.add( dateAccepted, "span 2, wrap" );

		layout.add( new Label("Date Available") );
		DatePicker dateAvailable = new DatePicker( LocalDate.now() );
		layout.add( dateAvailable, "span 2" );

		layout.add( new Label("Date Modified") );
		DatePicker dateModified = new DatePicker( LocalDate.now() );
		layout.add( dateModified, "span 2" );

		layout.add( new Label("Date Valid") );
		DatePicker dateValid = new DatePicker( LocalDate.now() );
		layout.add( dateValid, "span 2, wrap" );

		layout.add( new Label("Creator") );
		TextField creator = new TextField(  );
		layout.add( creator, "span 2" );

		layout.add( new Label("Contributor") );
		TextField contributor = new TextField(  );
		layout.add( contributor, "span 2" );

		layout.add( new Label("Publisher") );
		TextField publisher = new TextField(  );
		layout.add( publisher, "span 2, wrap" );

		layout.add( new Label("Conforms To"), "span 2" );
		layout.add( new Label("Has Part"), "span 2" );
		layout.add( new Label("Has Version"), "span 2, wrap" );

		TextField conformsTo = new TextField(  );
		layout.add( conformsTo, "span 2" );
		TextField hasPart = new TextField(  );
		layout.add( hasPart, "span 2" );
		TextField hasVersion = new TextField(  );
		layout.add( hasVersion, "span 2, wrap" );

		layout.add( new Label("Is Part Of"), "span 2" );
		layout.add( new Label("Is Referenced By"), "span 2" );
		layout.add( new Label("Is Replaced By"), "span 2, wrap" );

		TextField isPartOf = new TextField(  );
		layout.add( isPartOf, "span 2" );
		TextField isReferencedBy = new TextField(  );
		layout.add( isReferencedBy, "span 2" );
		TextField isReplacedBy = new TextField(  );
		layout.add( isReplacedBy, "span 2, wrap" );

		layout.add( new Label("Is Required By"), "span, wrap" );
		TextField isRequiredBy = new TextField(  );
		layout.add( isRequiredBy, "span, wrap" );

		layout.add( new Label("References"), "span 2" );
		layout.add( new Label("Relation"), "span 2" );
		layout.add( new Label("Replaces"), "span 2, wrap" );

		TextField references = new TextField(  );
		layout.add( references, "span 2" );
		TextField relation = new TextField(  );
		layout.add( relation, "span 2" );
		TextField replaces = new TextField(  );
		layout.add( replaces, "span 2, wrap" );

		layout.add( new Label("Requires"), "span 3" );
		layout.add( new Label("Source"), "span 3, wrap" );

		TextField requires = new TextField(  );
		layout.add( requires, "span 3" );
		TextField source = new TextField(  );
		layout.add( source, "span 3, wrap" );

		this.setContent( layout );

	}
}
