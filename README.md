# README

Ellie is a taxonomy manager made for classifiying files.

It is written in java and uses JavaFX.

It is able to connect to RDF/XML and Turtle files describing taxonomies, and can work with orientdb as a triple store.

## License

This software is licensed under Apache License version 2.
